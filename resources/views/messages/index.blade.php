{{ $message }}


@extends('layouts.app')

@section('title','Contact')

@section('content')
<h1 class="title">Message me:</h1>
@include('errors.list')
{{ Form::open() }}
<div class="form-group">
    {{ Form::label('name','Your name:') }}
    {{ Form::text('name',null, ["class"=>"form-controll"])}}
</div>
<div class="form-group">
    {{ Form::label('email','Email address:',["class"=>"form-controll"]) }}
    {{ Form::email('email',null, ["class"=>"form-controll"])}}
</div>
<div class="form-group">
    {{ Form::label('message',null,["class"=>"form-controll"]) }}
    {{ Form::textarea('message',null, ["class"=>"form-controll"])}}
</div>
{{Form::submit('send',["class"=>"btn btn-default"])}}
{{ Form::close() }}
@endsection