<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;    //Auth::user() -hez


class MessageController extends Controller
{
    public function index() {
        
        $currentUser = Auth::user();
        if ($currentUser) {
            $message = 'Hello, '.$currentUser->name.'!';
            return view('messages.index', compact('message'));
        } else {
            return redirect('/login');
        }
    }
}